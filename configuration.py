"""
The configuration service loads a configuration file into a
ConfigParser through which configuration values can be accessed
"""

from configparser import ConfigParser


class __Config:
    __path_to_config = '{}.config'

    @property
    def get(self) -> ConfigParser:
        """
        Gets a ConfigParser
        :return:
        """
        cp = ConfigParser()
        cp.read(self.__path_to_config.format('development'))
        return cp


__config = __Config()


def get() -> ConfigParser:
    return __config.get
