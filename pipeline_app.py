import sys
import argparse
import configuration
from typing import List
from pathlib import Path
from argparse import Namespace
from log_setup import get_logger
from utils import create_directory
from argparse import ArgumentParser
from configuration import ConfigParser
from pipeline_report import PipelineReport
from pipeline.transformation import Transformation
from pipeline.model_builder import Model

logger = get_logger(__name__)


class Pipeline:
    def __init__(self, config: ConfigParser):
        self.pipeline_report = PipelineReport()
        self.config = config
        self.data_folder_path = Path(config['General']['data directory'])
        self.transformed_folder_path = Path(config['General']['data transform directory'])
        self.model_plot_folder_path = Path(config['General']['model plot directory'])
        self.trained_model_folder_path = Path(config['General']['trained model directory'])
        self.predicted_data_folder_path = Path(config['General']['predicted data directory'])
        self.epoch = int(config['Training']['epoch'])

    def pipeline(self, arguments: List[str]) -> None:
        logger.info("+----------------------------------+")
        logger.info("| Pipeline started. |")
        logger.info("+----------------------------------+")

        self.pipeline_report.arguments = ' '.join(arguments)

        parser: ArgumentParser = argparse.ArgumentParser(description=__doc__)

        parser.add_argument("-T", "--transformation", help="download raw data files", action="store_true")
        parser.add_argument("-P", "--proposed_model", help="download raw data files", action="store_true")

        args: Namespace = parser.parse_args(args=arguments)

        create_directory(self.data_folder_path)
        create_directory(self.transformed_folder_path)
        create_directory(self.model_plot_folder_path)
        create_directory(self.trained_model_folder_path)

        try:
            if args.transformation:
                logger.info("************------------( Transformation started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Transformation")
                try:
                    transformer = Transformation(configuration.get())
                    logger.info("Data transformation")
                    transformer.transform()

                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.proposed_model:
                logger.info("************------------( Proposed Model training started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Proposed Model Building")
                try:
                    proposed_model = Model(configuration.get())
                    logger.info("Proposed model training")
                    proposed_model.proposed_model_build()
                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            self.pipeline_report.mark_success()
        except BaseException as e:
            self.pipeline_report.mark_failure()
            raise e

        finally:
            self.pipeline_report.log()


if __name__ == '__main__':
    pipeline: Pipeline = Pipeline(configuration.get())
    pipeline.pipeline(sys.argv[1:])
