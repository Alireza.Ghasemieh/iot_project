import random
import warnings
import math
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
from pandas import read_csv, concat
from pandas.core.common import SettingWithCopyWarning
from sklearn.preprocessing import MinMaxScaler
import configuration
from configuration import ConfigParser
from log_setup import get_logger

logger = get_logger(__name__)
warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=UserWarning)


class Transformation:
    def __init__(self, config: ConfigParser):
        self.config = config
        self.raw_folder_path = Path(config['General']['raw data directory'])
        self.transform_folder_path = Path(config['General']['data transform directory'])

    def transform(self):
        logger.info('------------------- 0. Importing Data -----------------')
        data = read_csv(f"{self.raw_folder_path}/dat.csv")
        pd.set_option("display.float", "{:.2f}".format)
        logger.info(data.info())
        logger.info(data.head())
        logger.info(data.describe())

        logger.info('------------------- 1. Creating Emergency Class ----------------------')

        index_list = data[(data['death.within.28.days'] == 1) |
                          (data['death.within.3.months'] == 1) |
                          (data['death.within.6.months'] == 1) |
                          (data['admission.way'] == 'Emergency') |
                          (data['return.to.emergency.department.within.6.months'] == 1) |
                          (data['DestinationDischarge'] == 'Died') |
                          (data['outcome.during.hospitalization'] == 'Dead')].index.tolist()
        logger.info(f' number of Emergency class: {len(index_list)}')

        data['Emergency'] = 0
        data.loc[index_list, 'Emergency'] = 1

        logger.info('------------------- 2. Remove unnecessary columns -----------------')
        drop_column = ['death.within.28.days',
                       'death.within.3.months',
                       'death.within.6.months',
                       'admission.way',
                       'return.to.emergency.department.within.6.months',
                       'DestinationDischarge',
                       'outcome.during.hospitalization',
                       'leukemia', 'Unnamed: 0', 'inpatient.number',
                       'malignant.lymphoma']
        data.drop(columns=drop_column, inplace=True, axis=1)

        logger.info('------------------- 3. Handling Missing Values ----------------------')
        logger.info(data.isna().sum())
        missing_values = self.missing_values_table(data)
        logger.info(missing_values)
        logger.info('Dropping columns that have more than 50% missing value ')
        self.dropna_threshold_col(data, 50, drop=True, verbose=False)
        logger.info(data.isna().sum())

        logger.info('Deleting rows of data from columns that have less han 5% missing value')
        self.dropna_threshold_row(data, 5, drop=True, verbose=False)
        data.reset_index(drop=True)

        missing_values = self.missing_values_table(data)
        logger.info(missing_values)

        logger.info('Fix missing values by assigning the mean of each feature for the missing values')

        miss_cols = ['left.ventricular.end.diastolic.diameter.LV', 'nucleotidase', 'fucosidase',
                     'total.bile.acid', 'glutamic.oxaloacetic.transaminase',
                     'creatine.kinase.isoenzyme.to.creatine.kinase',
                     'hydroxybutyrate.dehydrogenase.to.lactate.dehydrogenase', 'hydroxybutyrate.dehydrogenase',
                     'creatine.kinase', 'creatine.kinase.isoenzyme', 'lactate.dehydrogenase', 'cholesterol',
                     'low.density.lipoprotein.cholesterol', 'triglyceride', 'high.density.lipoprotein.cholesterol',
                     'D.dimer']

        for col_name in data.columns:
            if (col_name in miss_cols):
                data[col_name].fillna((data[col_name].mean()), inplace=True)

        logger.info(data.isna().sum())
        logger.info(data.info())

        logger.info('------------------- 4. Renaming Columns for better readability ----------------------')
        df_nm = data.copy()

        new_columns = {'body.temperature': 'BodyTemp',
                       'pulse': 'Pulse',
                       'respiration': 'Respiration',
                       'systolic.blood.pressure': 'Sys.BP',
                       'diastolic.blood.pressure': 'Dia.BP',
                       'map': 'Map',
                       'weight': 'Weight',
                       'left.ventricular.end.diastolic.diameter.LV': 'LeftVEDDLV',
                       'creatinine.enzymatic.method': 'CretEnzMethod',
                       'urea': 'Urea',
                       'uric.acid': 'UricAcid',
                       'glomerular.filtration.rate': 'GlmoFiltRate',
                       'cystatin': 'Cystatin',
                       'white.blood.cell': 'WBloodCell',
                       'red.blood.cell': 'RBloodCell',
                       'monocyte.ratio': 'MonocyteRatio',
                       'monocyte.count': 'MonocyteCount',
                       'coefficient.of.variation.of.red.blood.cell.distribution.width': 'RBldCelCoefVar',
                       'standard.deviation.of.red.blood.cell.distribution.width': 'RbldCelSD',
                       'mean.corpuscular.volume': 'MeanCorpVol',
                       'hematocrit': 'Hematocrit',
                       'lymphocyte.count': 'LymphocyteCnt',
                       'mean.hemoglobin.volume': 'MeanHemogVol',
                       'mean.hemoglobin.concentration': 'MeanHemogConc',
                       'mean.platelet.volume': 'MeanPlateVol',
                       'basophil.ratio': 'BasophilRatio',
                       'basophil.count': 'BasophilCnt',
                       'eosinophil.ratio': 'EosinophilRatio',
                       'eosinophil.count': 'EosinophilCnt',
                       'hemoglobin': 'Hemoglobin',
                       'platelet': 'Platelet',
                       'platelet.distribution.width': 'PlatDistWidth',
                       'platelet.hematocrit': 'PlatHematocrit',
                       'neutrophil.ratio': 'NeutrophilRatio',
                       'neutrophil.count': 'NeutrophilCnt',
                       'international.normalized.ratio': 'IntNormRatio',
                       'activated.partial.thromboplastin.time': 'ActPartThromTime',
                       'thrombin.time': 'ThrombinTime',
                       'prothrombin.activity': 'ProthrombinAct',
                       'prothrombin.time.ratio': 'ProthrombinTmRt',
                       'fibrinogen': 'Fibrinogen',
                       'high.sensitivity.troponin': 'HiSensTroponin',
                       'carbon.dioxide.binding.capacity': 'CarbDioxBinCap',
                       'calcium': 'Calcium',
                       'potassium': 'Potassium',
                       'chloride': 'Chloride',
                       'sodium': 'Sodium',
                       'creatine.kinase.isoenzyme.to.creatine.kinase': 'CreKiIsoCreaKi',
                       'hydroxybutyrate.dehydrogenase.to.lactate.dehydrogenase': 'HDtoLacDeh',
                       'hydroxybutyrate.dehydrogenase': 'HydroxDehydro',
                       'glutamic.oxaloacetic.transaminase': 'GlutamicOxaTrans',
                       'creatine.kinase': 'CreatineKinase',
                       'creatine.kinase.isoenzyme': 'CreatKinIso',
                       'lactate.dehydrogenase': 'LactateDehdro',
                       'brain.natriuretic.peptide': 'BrianNatrPeti',
                       'nucleotidase': 'Nucleotidase',
                       'fucosidase': 'Fucosidase',
                       'albumin': 'Albumin',
                       'white.globulin.ratio': 'WhtGlobRt',
                       'glutamyltranspeptidase': 'Glutamyltrans',
                       'glutamic.pyruvic.transaminase': 'GlutPyruvicTran',
                       'indirect.bilirubin': 'IndirectBilirubin',
                       'alkaline.phosphatase': 'AlkalinePhosph',
                       'globulin': 'Globulin',
                       'direct.bilirubin': 'DirectBiliru',
                       'total.bilirubin': 'TtlBilirubin',
                       'total.bile.acid': 'TtlBileAcid',
                       'total.protein': 'TotalProtein',
                       'cholesterol': 'Cholesterol',
                       'low.density.lipoprotein.cholesterol': 'LwDenLopoChol',
                       'triglyceride': 'Triglyceride',
                       'high.density.lipoprotein.cholesterol': 'HiDensLipoChol',
                       'dischargeDay': 'DischargeDay',
                       'admission.ward': 'AdmissionWard',
                       'occupation': 'Occupation',
                       'discharge.department': 'DischargeDep',
                       'visit.times': 'VisitTimes',
                       'gender': 'Gender',
                       'type.of.heart.failure': 'TypHeartFail',
                       'NYHA.cardiac.function.classification': 'NYHACardClass',
                       'Killip.grade': 'KillipGrade',
                       'myocardial.infarction': 'MyocardInfar',
                       'congestive.heart.failure': 'CongeHeartFail',
                       'peripheral.vascular.disease': 'PeripVascDis',
                       'cerebrovascular.disease': 'CerebroVascuDis',
                       'dementia': 'Dementia',
                       'Chronic.obstructive.pulmonary.disease': 'ChroObstrPulmoD',
                       'connective.tissue.disease': 'ConnTissD',
                       'peptic.ulcer.disease': 'PepticUlcerD',
                       'diabetes': 'Diabetes',
                       'moderate.to.severe.chronic.kidney.disease': 'MdToSvChronKidnD',
                       'hemiplegia': 'Hemiplegia',
                       'solid.tumor': 'SolidTumor',
                       'liver.disease': 'LiverDisease',
                       'type.II.respiratory.failure': 'Typ2RespFail',
                       'consciousness': 'Consciousness',
                       'eye.opening': 'EyeOpening',
                       'verbal.response': 'VerbalResp',
                       'movement': 'Movement',
                       'respiratory.support.': 'RespiratSprt',
                       'oxygen.inhalation': 'OxygenInhal',
                       're.admission.within.28.days': 'ReAdmmin28Days',
                       're.admission.within.3.months': 'ReAdmmin3Mnts',
                       're.admission.within.6.months': 'ReAdmmin6Mnts',
                       'ageCat': 'AgeCategory'}

        df_nm.rename(columns=new_columns, inplace=True)

        logger.info('Generate a list of categorical and numerical features')

        categorical_val = []
        continous_val = []
        for column in df_nm.columns:
            if len(df_nm[column].unique()) < 10:
                categorical_val.append(column)
            else:
                continous_val.append(column)

        logger.info("check the datatype of continuous to make sure it's all numeric")
        logger.info(list(set((df_nm[continous_val]).dtypes.to_list())))


        logger.info('------------------- 5. Handling outliers ----------------------')
        df_out = df_nm.copy()
        logger.info(f' df_z.shape : {df_out.shape}')

        logger.info("Get a list of features that need to be checked for outliers")
        outlier_col_list = df_out[continous_val].columns
        logger.info(outlier_col_list)

        logger.info(f'df_out shape: {df_out.shape}')

        logger.info("Drop elements falling outside of quantiles")
        idx_lst = []
        for col_name in df_out.columns:
            if (col_name in outlier_col_list):
                q_low = df_out[col_name].quantile(0.01)
                q_high = df_out[col_name].quantile(0.99)
                row_idx = df_out[(df_out[col_name] < q_low) | (df_out[col_name] > q_high)].index
                for idx in row_idx:
                    if idx not in idx_lst:
                        idx_lst.append(idx)

        df_out.drop(idx_lst, inplace=True)
        df_out.reset_index(drop=True)

        logger.info(f'number of elements dropped: {len(idx_lst)}')

        logger.info(f'df_out new shape: {df_out.shape}')

        logger.info("Check if there are any features that have only one class of values")

        drop_col = []
        for col in df_out.columns:
            uv = len(df_out[col].unique())
            if (uv == 1):
                drop_col.append(col)
                logger.info(f'column {col} only has one class - dropping it!')

        df_out.drop(columns=drop_col, inplace=True, axis=1)

        logger.info(f'Emergency value_counts() \n {df_out["Emergency"].value_counts()}')

        logger.info('------------------- 6. Normalize Data ----------------------')

        df_norm = df_out.copy()
        logger.info(df_norm.head())

        df_norm = self.scaleColumns(df_norm, continous_val)

        logger.info(f' df_norm.shape : {df_norm.shape}')

        logger.info(df_norm.head())

        logger.info('------------------- 7. Create Dummy Variables ----------------------')

        df_dummy = df_norm.copy()

        logger.info("Convert all categorical features to string before dummy creation")

        for column in df_dummy.select_dtypes(include=['integer', 'float']).columns:
            if ((column in categorical_val) & (column != 'Emergency')):
                df_dummy[column] = df_dummy[column].astype(int).astype(str)

        logger.info(df_dummy.info())

        logger.info('create df_dummy from df_norm with dummy variables')
        df_dummy = pd.get_dummies(data=df_dummy, drop_first=True)

        logger.info(df_dummy.info())

        logger.info('------------------- 8. Remove highly collinear features -----------------')
        final_dataset = df_dummy.reindex(columns=(['Emergency'] + list([a for a in df_dummy.columns if a != 'Emergency'])))
        logger.info(final_dataset.info())

        dataset_preprocessed = self.remove_collinear_features(final_dataset, 0.4, 'Emergency', True)

        logger.info('------------------- 9. Perform Backward Elimination -----------------')

        selected_columns = dataset_preprocessed.columns[1:].values
        data_modeled, selected_columns = self.backwardElimination(dataset_preprocessed.iloc[:, 1:].values,
                                                                  dataset_preprocessed.iloc[:, 0].values,
                                                                  0.05, selected_columns, verbose=True)
        selected_columns = np.append(selected_columns, "Emergency", axis=None)
        dataset_selected = dataset_preprocessed[selected_columns]

        logger.info('------------------- 10. Balance the dataset -----------------')
        from sklearn.utils import resample
        # Separate majority and minority classes
        df_majority = dataset_selected[dataset_selected.Emergency == 1]
        df_minority = dataset_selected[dataset_selected.Emergency == 0]

        # Upsample minority class
        df_minority_upsampled = resample(df_minority,
                                         replace=True,  # sample with replacement
                                         n_samples=(len(df_majority)-len(df_minority)),  # to match majority class
                                         random_state=123)  # reproducible results

        # Combine majority class with upsampled minority class
        df_upsampled = pd.concat([df_majority, df_minority_upsampled, df_minority])
        df_upsampled = df_upsampled.sample(frac=1).reset_index(drop=True)
        # Display new class counts
        logger.info(df_upsampled.Emergency.value_counts())

        logger.info('------------------- 11. Saving Cleaned Dataset -----------------')

        df_upsampled.to_csv(f'{self.transform_folder_path}/dat_cleaned.csv', encoding='utf-8', index=False)

    # -----------------------------------------------------------------------------------------------------------------
    # Function to normalize column using MinMaxScaler()
    @staticmethod
    def scaleColumns(df, cols_to_scale):
        for col in cols_to_scale:
            df[col] = MinMaxScaler().fit_transform(np.array(df[col]).reshape(-1, 1))

        return df

    @staticmethod
    def backwardElimination(x, Y, significant_level, columns, verbose=False):
        numVars = len(x[0])
        for i in range(0, numVars):
            regressor_OLS = sm.OLS(Y, x).fit()
            maxVar = max(regressor_OLS.pvalues).astype(float)
            logger.info(f'---> i: {i} -  Max p.value is: {maxVar}')
            if maxVar > significant_level:
                for j in range(0, numVars - i):
                    if regressor_OLS.pvalues[j].astype(float) == maxVar:
                        x = np.delete(x, j, 1)
                        columns = np.delete(columns, j)

        logger.info(regressor_OLS.summary())
        logger.info(f"\n{len(columns)} Selected columns are:\n {columns} ")

        return x, columns

    @staticmethod
    def remove_collinear_features(df_model, threshold, target_var, verbose):
        """
        Objective:
            Remove collinear features in a dataframe with a correlation coefficient
            greater than the threshold and which have the least correlation with the target (dependent) variable. Removing collinear features can help a model
            to generalize and improves the interpretability of the model.

        Inputs:
            df_model: features dataframe
            target_var: target (dependent) variable
            threshold: features with correlations greater than this value are removed
            verbose: set to "True" for the log printing

        Output:
            dataframe that contains only the non-highly-collinear features
        """

        logger.info(f"Dataset Shape before elimination {df_model.shape}")

        # Calculate the correlation matrix
        corr_matrix = df_model.drop(target_var, 1).corr()
        iters = range(len(corr_matrix.columns) - 1)
        drop_cols = []
        dropped_feature = ""

        # Iterate through the correlation matrix and compare correlations
        for i in iters:
            for j in range(i + 1):
                item = corr_matrix.iloc[j:(j + 1), (i + 1):(i + 2)]
                col = item.columns
                row = item.index
                val = abs(item.values)

                # If correlation exceeds the threshold
                if val >= threshold:
                    # Print the correlated features and the correlation value
                    col_value_corr = df_model[col.values[0]].corr(df_model[target_var])
                    row_value_corr = df_model[row.values[0]].corr(df_model[target_var])
                    if col_value_corr < row_value_corr:
                        drop_cols.append(col.values[0])
                        dropped_feature = "dropped: " + col.values[0]
                    else:
                        drop_cols.append(row.values[0])
                        dropped_feature = "dropped: " + row.values[0]
                    if verbose:
                        logger.info(dropped_feature)
                        logger.info("-----------------------------------------------------------------------------")

        # Drop one of each pair of correlated columns
        drops = set(drop_cols)
        df_model.drop(columns=drops, axis=1, inplace=True)

        logger.info("dropped columns: ")
        logger.info(list(drops))
        logger.info("-----------------------------------------------------------------------------")
        logger.info("used columns: ")
        logger.info(df_model.columns.to_list())

        logger.info(f"Dataset Shape after elimination {df_model.shape}")

        return df_model

    @staticmethod
    def fix_missing_mean_groupby(df, col_name, grouping_name):
        for column in df:
            if column in col_name:
                # if person is smoker then fill the NaN value with the median of the group it falls into
                df[column].fillna(df.groupby(grouping_name)[column].transform('median'), inplace=True)

                # above doesn't replace NaN for people who are not smoker, because they don't smoke so the smokeage
                #  is missing... therefore let's replace that NaN with 0
                df[column].fillna(0, inplace=True)

    @staticmethod
    def fix_missing_prop_replacement(df, col_name):
        for column in df:
            if column in col_name:
                logger.info(f'value_counts() for {column} before assignment: {df[column].value_counts()}')

                # Calculating probability and expected value
                null_count = df[column].isnull().sum()
                # proportion = np.array(df[column].value_counts().values) / df[column].value_counts().sum() * null_count
                prop = np.around(np.array(df[column].value_counts().values) / df[
                    column].value_counts().sum() * null_count).astype('int')

                # Adjusting proportion
                diff = int(null_count - np.sum(prop))
                if diff > 0:
                    for x in range(diff):
                        idx = random.randint(0, len(prop) - 1)
                        prop[idx] = prop[idx] + 1
                else:
                    diff = -diff
                    while diff != 0:
                        idx = random.randint(0, len(prop) - 1)
                        if prop[idx] > 0:
                            prop[idx] = prop[idx] - 1
                            diff = diff - 1

                # Filling missing values
                nan_indexes = df[df[column].isnull()].index.tolist()
                for x in range(len(prop)):
                    if prop[x] > 0:
                        random_subset = random.sample(population=nan_indexes, k=prop[x])
                        df.loc[random_subset, column] = df[column].value_counts().keys()[x]
                        nan_indexes = list(set(nan_indexes) - set(random_subset))

                logger.info(f'value_counts() for  {column}  after assignment: {df[column].value_counts()}')

    @staticmethod
    def fix_missing_mode_groupby(df, col_name, grouping_name):
        for column in df:
            if column in col_name:
                df[column] = df.groupby(grouping_name)[column].apply(lambda x: x.fillna(x.mode()[0]))
                if column in ['curruse', 'everuse']:
                    df[column] = df.groupby(grouping_name)[column].apply(lambda x: x.replace(99999, x.mode()[0]))

    @staticmethod
    def fix_missing_col_mode(df, col_name):
        for column in df:
            if column in col_name:
                df[column].fillna(df[column].value_counts().index[0], inplace=True)

    # funciton to print list of columns that have missing value greater than a threashold
    # if drop=True, then the rows with missing values in the columns detected will be dropped inplace

    @staticmethod
    def dropna_threshold(df, threshold_pct, drop=False):
        row_num = df.shape[0]
        dropna_cols = []
        for column in df.columns.to_list():
            miss_val_count = df[column].isnull().sum()
            pct = miss_val_count / row_num
            if (miss_val_count > 0) & (pct < threshold_pct):
                logger.info('column ' + column +
                            ' - missing count:' + str(miss_val_count) +
                            ' pct: ' + str((pct * 100).round(1)) + '% missing')
                dropna_cols.append(column)
        if drop:
            df.dropna(axis='index', subset=dropna_cols, inplace=True)
            logger.info('Missing rows from following columns are deleted from dataset.')
            logger.info(dropna_cols)

    # Function to drop columns that have missing values more than a threshold
    @staticmethod
    def dropna_threshold_col(df, threshold_pct, drop=False, verbose=False):
        na_obj = df.isnull().sum()
        dropna_cols = []
        for key, value in na_obj.iteritems():
            pct = round((value / len(df) * 100), 2)
            if (value > 0) & (round(pct) >= threshold_pct):
                # if verbose: logger.info(key, ",", pct)
                dropna_cols.append(key)

        if drop:
            df.drop(columns=dropna_cols, inplace=True)
            # if verbose: logger.info('\n\nMissing rows from following columns are deleted from dataset.\n')
            # if verbose: logger.info(dropna_cols)

    # dropping missing rows in columns that have less than 5% missing
    @staticmethod
    def dropna_threshold_row(df, threshold_pct, drop=False, verbose=False):
        na_obj = df.isnull().sum()
        dropna_cols = []
        for key, value in na_obj.iteritems():
            pct = round((value / len(df) * 100), 2)
            if (value > 0) & (round(pct) <= threshold_pct):
                # if verbose: logger.info(key, ",", pct)
                dropna_cols.append(key)

        if drop:
            df.dropna(axis='index', subset=dropna_cols, inplace=True)
            # if verbose: logger.info('\n\nMissing rows from following columns are deleted from dataset.\n')
            # if verbose: logger.info(dropna_cols)

    # Function to calculate missing values by column# Funct
    @staticmethod
    def missing_values_table(df):
        # Total missing values
        mis_val = df.isnull().sum()

        # Percentage of missing values
        mis_val_percent = 100 * df.isnull().sum() / len(df)

        # Make a table with the results
        mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)

        # Rename the columns
        mis_val_table_ren_columns = mis_val_table.rename(
            columns={0: 'Missing Values', 1: '% of Total Values'})

        # Sort the table by percentage of missing descending
        mis_val_table_ren_columns = mis_val_table_ren_columns[
            mis_val_table_ren_columns.iloc[:, 1] != 0].sort_values(
            '% of Total Values', ascending=False).round(1)

        # Print some summary information
        logger.info(f"Dataframe has {str(df.shape[1])} columns")
        logger.info(f"There are {str(mis_val_table_ren_columns.shape[0])} columns that have missing values")

        # Return the dataframe with missing information
        return mis_val_table_ren_columns

    @staticmethod
    def missing_values_table(df):
        # Total missing values
        df = df.replace('?', np.NaN)
        mis_val = df.isnull().sum()

        # Percentage of missing values
        mis_val_percent = 100 * df.isnull().sum() / len(df)

        # Make a table with the results
        mis_val_table = concat([mis_val, mis_val_percent], axis=1)

        # Rename the columns
        mis_val_table_ren_columns = mis_val_table.rename(
            columns={0: 'Missing Values', 1: '% of Total Values'})

        # Sort the table by percentage of missing descending
        mis_val_table_ren_columns = mis_val_table_ren_columns[
            mis_val_table_ren_columns.iloc[:, 1] != 0].sort_values(
            '% of Total Values', ascending=False).round(1)

        # Print some summary information
        logger.info(f"Dataframe has {str(df.shape[1])} columns.")
        logger.info(f"There are {str(mis_val_table_ren_columns.shape[0])} columns that have missing values.")

        # Return the dataframe with missing information
        logger.info(f"{mis_val_table_ren_columns}")
        return mis_val_table_ren_columns


if __name__ == '__main__':
    transform: Transformation = Transformation(configuration.get())
    transform.transform()
