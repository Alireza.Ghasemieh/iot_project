import pickle
from datetime import datetime
from pathlib import Path
import pandas as pd
from numpy import mean
from numpy import std
from sklearn import model_selection
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import StackingClassifier, VotingClassifier, GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import RepeatedStratifiedKFold, cross_validate
from sklearn.model_selection import cross_val_predict
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from xgboost import XGBClassifier
from sklearn.metrics import roc_auc_score
from sklearn.metrics import plot_confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.metrics import plot_roc_curve
from sklearn.model_selection import StratifiedKFold
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import precision_recall_curve, auc
import configuration
from configuration import ConfigParser
from log_setup import get_logger

logger = get_logger(__name__)


class Model:
    def __init__(self, config: ConfigParser):
        self.config = config
        self.model_plot_folder_path = Path(config['General']['model plot directory'])
        self.trained_model_folder_path = Path(config['General']['trained model directory'])
        self.splitting_percentage = float(config['Preprocessing']['splitting_percentage'])
        self.read_dataset_number_rows = int(config['Training']['number rows'])

    def proposed_model_build(self):
        # define dataset
        X, y = self.get_dataset(filename='data/transformed/dat_cleaned.csv', nrows=self.read_dataset_number_rows)
        # get the models to evaluate
        models = self.get_models()
        # evaluate the models and store results
        logger.info('>> Start Time: %s' % (datetime.now()))
        results_recall, results_accuracy, names = list(), list(), list()
        for name, my_model in models.items():
            logger.info(f"Model {name} is training")
            # self.train_model(model_name=name, my_model=my_model, X=X, Y=y)
            scores = self.evaluate_model(model_name=name, classifier=my_model, X=X, y=y)
            results_recall.append(scores['test_recall'])
            results_accuracy.append(scores['test_accuracy'])
            names.append(name)
            logger.info('--------------------------------------------------------------------------')

        # self.MLP(X, y) # Todo

        # plot model performance for comparison
        plt.boxplot(results_recall, labels=names, showmeans=True, )
        plt.xticks(rotation=45)
        plt.title('Models Recall')
        plt.tight_layout()
        plt.savefig(f'model_plot/All_models_Recall.png')
        plt.show()

        plt.boxplot(results_accuracy, labels=names, showmeans=True)
        plt.xticks(rotation=45)
        plt.title('Models Accuracy')
        plt.tight_layout()
        plt.savefig(f'model_plot/All_models_Accuracy.png')
        plt.show()

    # get the dataset
    @staticmethod
    def get_dataset(filename, nrows=None):
        logger.info(f"Number of data row is {nrows}")
        data = pd.read_csv(filename, nrows=nrows)
        X = data.iloc[:, :-1].values
        y = data.iloc[:, -1].values
        return X, y

    # get a list of models to evaluate
    def get_models(self):
        models = dict()
        models['logistic_regression'] = self.logistic_regression()
        models['knn'] = self.KNN()
        models['decision_tree'] = self.decision_tree()
        models['random_forest'] = self.random_forest()
        models['svm'] = self.SVM()
        models['naive_bayes'] = self.naive_bayes()
        models['sgd'] = self.SGD()
        models['xgboost'] = self.XGBoost()
        models['stacking'] = self.get_stacking()
        models['Voting Classifier'] = self.get_VotingClassifier
        models['Gradient Boosting'] = self.get_GradientBoostingClassifier()
        return models

    @staticmethod
    def train_model(model_name, my_model, X, Y):
        test_size = 0.33
        seed = 7
        X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X, Y, test_size=test_size,
                                                                            random_state=seed)
        # Fit the model on training set
        my_model.fit(X_train, Y_train)
        # save the model to disk
        filename = f'trained_model/{model_name}_model.sav'
        pickle.dump(my_model, open(filename, 'wb'))

    # evaluate a give model using cross-validation

    def evaluate_model(self, model_name, classifier, X, y):
        from sklearn.metrics import balanced_accuracy_score
        from sklearn.metrics import f1_score
        cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)

        self.roc(model_name=model_name, classifier=classifier, X=X, y=y)
        self.precision_recall(model_name=model_name, classifier=classifier, X=X, y=y)
        scores = cross_validate(classifier, X, y, scoring=['accuracy', 'recall'], cv=cv, n_jobs=-1, error_score='raise')
        self.confusion_matrix(model_name=model_name, classifier=classifier, X=X, y=y)
        logger.info(
            f"Model {model_name} recall is {round(mean(scores['test_recall']), 4)}, ({round(std(scores['test_recall']), 4)})")
        logger.info(
            f"Model {model_name} accuracy is {round(mean(scores['test_accuracy']), 4)}, ({round(std(scores['test_accuracy']), 4)})")
        # Balance accuracy
        y_pred = cross_val_predict(classifier, X, y, cv=10)
        balanced_accuracy = balanced_accuracy_score(y, y_pred)
        logger.info(f"Model {model_name} balanced accuracy is {round(balanced_accuracy, 4)}")
        # f1_score
        f1_score = f1_score(y, y_pred)
        logger.info(f"Model {model_name} f1 score is {round(f1_score, 4)}")
        return scores

    @staticmethod
    def confusion_matrix(model_name, classifier, X, y):
        # Confusion Matrix
        y_pred = cross_val_predict(classifier, X, y, cv=10)
        tn, fp, fn, tp = confusion_matrix(y, y_pred).ravel()
        logger.info(f"Model {model_name} confusion matrix (tn, fp, fn, tp) score is {(tn, fp, fn, tp)}")

        X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
        np.set_printoptions(precision=2)
        class_names = [0, 1]
        # Plot non-normalized confusion matrix
        titles_options = [(f"{model_name} Confusion matrix, without normalization", None),
                          (f"{model_name} Normalized confusion matrix", 'true')]
        for title, normalize in titles_options:
            disp = plot_confusion_matrix(classifier, X_test, y_test,
                                         display_labels=class_names,
                                         cmap=plt.cm.Blues,
                                         normalize=normalize)
            disp.ax_.set_title(title)
            plt.savefig(f'model_plot/{model_name}-{normalize}_CM.png')
            plt.show()

    @staticmethod
    def roc(model_name, classifier, X, y):
        cv = StratifiedKFold(n_splits=6)
        tprs = []
        aucs = []
        mean_fpr = np.linspace(0, 1, 100)

        fig, ax = plt.subplots()
        for i, (train, test) in enumerate(cv.split(X, y)):
            classifier.fit(X[train], y[train])
            viz = plot_roc_curve(classifier, X[test], y[test], name='ROC fold {}'.format(i), alpha=0.3, lw=1, ax=ax)
            interp_tpr = np.interp(mean_fpr, viz.fpr, viz.tpr)
            interp_tpr[0] = 0.0
            tprs.append(interp_tpr)
            aucs.append(viz.roc_auc)

        ax.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r', label='Chance', alpha=.8)

        mean_tpr = np.mean(tprs, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr, mean_tpr)
        std_auc = np.std(aucs)
        ax.plot(mean_fpr, mean_tpr, color='b', label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc), lw=2,
                alpha=.8)

        std_tpr = np.std(tprs, axis=0)
        tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
        tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
        ax.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2, label=r'$\pm$ 1 std. dev.')

        ax.set(xlim=[-0.05, 1.05], ylim=[-0.05, 1.05], title=f"ROC of {model_name} model")
        ax.legend(loc="lower right")
        fig.tight_layout()
        plt.savefig(f'model_plot/{model_name}_ROC.png')
        plt.show()
        # ROC
        y_pred = cross_val_predict(classifier, X, y, cv=10)
        roc = roc_auc_score(y, y_pred)
        logger.info(f"Model {model_name} ROC is {round(roc, 4)}")

    @staticmethod
    def precision_recall(model_name, classifier, X, y):
        cv = StratifiedKFold(n_splits=10)
        y_real = []
        y_proba = []

        fig, ax = plt.subplots()
        for i, (train, test) in enumerate(cv.split(X, y)):
            classifier.fit(X[train], y[train])
            y_pred = classifier.predict(X[test])
            precision, recall, _ = precision_recall_curve(y[test], y_pred)
            lab = 'Fold %d AUC=%.4f' % (i + 1, auc(recall, precision))
            ax.step(recall, precision, label=lab)
            y_real.append(y[test])
            y_proba.append(y_pred)

        y_real = np.concatenate(y_real)
        y_proba = np.concatenate(y_proba)
        precision, recall, _ = precision_recall_curve(y_real, y_proba)
        lab = 'Overall AUC=%.4f' % (auc(recall, precision))
        ax.step(recall, precision, label=lab, lw=2, color='b')
        ax.set_xlabel('Recall')
        ax.set_ylabel('Precision')
        ax.legend(loc='lower left', fontsize='small')
        ax.set(xlim=[-0.05, 1.05], ylim=[-0.05, 1.05], title=f"Recall-Precision of {model_name} model")
        fig.tight_layout()
        plt.savefig(f'model_plot/{model_name}_Recall_Precision.png')
        plt.show()

    @staticmethod
    def get_GradientBoostingClassifier():
        my_model = GradientBoostingClassifier(n_estimators=100, learning_rate=1.0, max_depth=100, random_state=0)
        return my_model

    @property
    def get_VotingClassifier(self):
        estimators = list()
        estimators.append(('logistic_regression', self.logistic_regression()))
        estimators.append(('knn', self.KNN()))
        estimators.append(('decision_tree', self.decision_tree()))
        estimators.append(('random_forest', self.random_forest()))
        estimators.append(('svm', self.SVM()))
        # estimators.append(('naive_bayes', self.naive_bayes()))
        # estimators.append(('sgd', self.SGD()))
        estimators.append(('xgboost', self.XGBoost()))

        # define the stacking ensemble
        my_model = VotingClassifier(estimators=estimators, voting='soft')
        return my_model

    # get a stacking ensemble of models
    def get_stacking(self):
        # define the base models
        level0 = list()
        level0.append(('logistic_regression', self.logistic_regression()))
        level0.append(('knn', self.KNN()))
        level0.append(('decision_tree', self.decision_tree()))
        level0.append(('random_forest', self.random_forest()))
        level0.append(('svm', self.SVM()))
        # level0.append(('naive_bayes', self.naive_bayes()))
        # level0.append(('sgd', self.SGD()))
        level0.append(('xgboost', self.XGBoost()))

        # define meta learner model
        level1 = self.XGBoost()
        # define the stacking ensemble
        my_model = StackingClassifier(estimators=level0, final_estimator=level1, cv=5)
        return my_model

    @staticmethod
    def SGD():
        my_model = SGDClassifier(tol=0.01, max_iter=5000)
        return my_model

    @staticmethod
    def random_forest():
        my_model = RandomForestClassifier(random_state=0, n_jobs=-1, n_estimators=100, max_depth=3)
        return my_model

    @staticmethod
    def SVM():
        my_model = SVC(probability=True)
        return my_model

    @staticmethod
    def logistic_regression():
        my_model = LogisticRegression(max_iter=1000000)
        return my_model

    @staticmethod
    def decision_tree():
        my_model = DecisionTreeClassifier(random_state=0, max_depth=3)
        return my_model

    @staticmethod
    def naive_bayes():
        my_model = GaussianNB()
        return my_model

    @staticmethod
    def KNN():
        my_model = KNeighborsClassifier(n_neighbors=5, n_jobs=-1)
        return my_model

    @staticmethod
    def XGBoost():
        my_model = XGBClassifier(random_state=0, n_jobs=-1, learning_rate=0.1, n_estimators=100, max_depth=3,
                                 objective='binary:logistic', booster='gbtree')
        return my_model

    @staticmethod
    def MLP(X, y):
        from sklearn.model_selection import train_test_split
        logger.info("MLP Training Started")
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
        # Initializing the ANN
        ann = tf.keras.models.Sequential()
        # Adding the input layer and the first hidden layer
        ann.add(tf.keras.layers.Dense(units=15, activation='relu'))
        # Adding the output layer
        ann.add(tf.keras.layers.Dense(units=1, activation='softmax'))

        ann.compile(optimizer='adam', metrics=['accuracy', tf.keras.metrics.Precision(), tf.keras.metrics.Recall()],
                    loss='categorical_crossentropy')
        ann.fit(X_train, y_train, batch_size=10, epochs=100, verbose=0)
        y_pred = ann.predict(X_test)
        np.set_printoptions(precision=2)

        tn, fp, fn, tp = confusion_matrix(y_test, y_pred).ravel()
        roc = roc_auc_score(y_test, y_pred)
        logger.info(f"Model MLP confusion matrix (tn, fp, fn, tp) score is {(tn, fp, fn, tp)}")
        logger.info(f"Model MLP ROC is {round(roc, 4)}")


if __name__ == '__main__':
    model: Model = Model(configuration.get())
    model.proposed_model_build()
