# IoT Project

This project is a prediction engine

## Setup

We will use Python 3.8 Confirm that your version of python by running python --version. Note that you might have both
python 2 and python 3 installed, and that python 3 might be aliased to ```python3```.

If using Windows, ensure that Microsoft Visual Studio C++ build tools are installed https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2019

We will be using a python Virtual Environment to manage project dependency versions.
For information on setting up a Python Virtual Environment, see https://sourabhbajaj.com/mac-setup/Python/virtualenv.html

Create a folder for a new vitual environment where dependencies will be stored. 

It is recommended to call it '<project-root>/.env' (.gitignore is already configured to ignore this folder)

```
$ python -m venv <project-root>/.env
```

Activate the environment (only necessary if not using Pycharm)
* Windows
    ```
    $ <project-root>/.env/Scripts/activate
    ```
* OSX & Linux    
    ```
    $ source <project-root>/.env/bin/activate
    ```
* From the project root, install the project dependencies 
    ```
    $ pip install -r requirements.txt
    ```
    If you get an SSL error, enjoy the life-changing magic of reading [this confluence page](https://confluence.i-proving.com/display/TOKB/Configuring+your+workstation+for+SSL+Content+Inspection)


If you are using PyCharm, be sure to use the Virtual Environment as the Project Interpreter.
This will allow PyCharm to load the necessary packages, as well as control package dependencies for you.

Windows:
* File > Settings > Project > Project Interpreter

OSX & Linux:
* PyCharm > Preferences > Project Interpreter 

See https://www.jetbrains.com/help/pycharm/configuring-python-interpreter.html

# Running the Pipeline

* `-T` switch is used to transform the dataset

* `-P` switch is used to train the proposed model

#### Command Line
```buildoutcfg
python pipeline_app.py -T -P
```